package com.lc.mybatisx.parse;

public enum KeywordType {

    ACTION, NONE, LINK, WHERE, ORDER, FUNC, LIMIT

}
