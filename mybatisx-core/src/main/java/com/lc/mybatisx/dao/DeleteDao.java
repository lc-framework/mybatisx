package com.lc.mybatisx.dao;

import com.lc.mybatisx.annotation.MapperMethod;
import com.lc.mybatisx.annotation.MethodType;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

/**
 * @author ：薛承城
 * @description：删除dao
 * @date ：2020/7/20 14:46
 */
public interface DeleteDao<ENTITY, ID extends Serializable> extends Dao {

    // @MapperMethod(type = MethodType.DELETE)
    int deleteById(@Param("id") ID id);

}
