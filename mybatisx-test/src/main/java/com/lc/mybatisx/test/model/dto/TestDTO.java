package com.lc.mybatisx.test.model.dto;

public class TestDTO {

    private Long id;

    private String name;

    private String username;

    private String password;

    private String payStatus;

    private String payStatusXyz;

    private String payStatusXyzAbc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPayStatusXyz() {
        return payStatusXyz;
    }

    public void setPayStatusXyz(String payStatusXyz) {
        this.payStatusXyz = payStatusXyz;
    }

    public String getPayStatusXyzAbc() {
        return payStatusXyzAbc;
    }

    public void setPayStatusXyzAbc(String payStatusXyzAbc) {
        this.payStatusXyzAbc = payStatusXyzAbc;
    }
}
