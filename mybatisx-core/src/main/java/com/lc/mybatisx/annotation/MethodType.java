package com.lc.mybatisx.annotation;

public enum MethodType {

    INSERT, DELETE, UPDATE, QUERY

}
